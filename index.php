<?php

# Namespaces ==>

require "./app.php"; // use app\Product;
require "./data.php"; //use data\Product;

$product_one = new app\Product;
$product_one = new data\Product;



# Functions ==>
// !1.Defining and Invoking Functions: ==>

function praisePHP(){
  echo "praising the PHP language";
}

function inflateEgo()
  {
    echo "PHP is a good lng.";
  }

inflateEgo();

// !2.Return Statements: ==>

function printStringReturnNumber(){
  echo "Six";
  return 6;
  }

$my_num = printStringReturnNumber();
echo " " .$my_num;

function first()
{
  return "You did it!\n";
}

function second()
{
  return "You're amazing!\n";
}

function third()
{
  return "You're a hero!\n";
}    

echo first(), second(), third();

// !3.Parameters: ==>

function increaseEnthusiasm($str)
{
  return $str . "!";
}
echo increaseEnthusiasm("hi");

function repeatThreeTimes($arg){
  return $arg . $arg . $arg;
}

echo repeatThreeTimes(".");
echo increaseEnthusiasm(repeatThreeTimes(" World"));

function calculateArea($w, $h){
  return "rectangle area is: " . $w * $h ."\n";
}
echo calculateArea(5, 4);

function calculateVolume($w, $h, $d){
  return "box volume is: " . $w * $h * $d ."\n";
}
echo calculateVolume(5, 4, 3);

// *default Parameter: -->
function calculateTip($tip, $off = 20){
  return $tip * (1 + ($off/100));
}
echo calculateTip(120);
echo calculateTip(120, 25);

$string_one = "you have teeth";
$string_two = "toads are nice";
$string_three = "brown is my favorite color";

function convertToQuestion(&$question){
  $question = "Do you think " . $question . "?" . "\n"; 
}

convertToQuestion($string_one);
echo $string_one;
convertToQuestion($string_two);
echo $string_two;
convertToQuestion($string_three);
echo $string_three;

$language = "PHP";
$topic = "scope";

function generateLessonName($concept) //Scope
{
  global $language;
  return $language . ": " . $concept;
}

echo generateLessonName($topic);

// !Built-in Functions: ==>

$first = "Welcome to the magical world of built-in functions.";
$second = 82137012983; 

echo gettype($first);
var_dump($first);
echo gettype($second);
var_dump($second);

// *String Functions: -->

//original string in reverse.
echo strrev(".pu ti peeK .taerg gniod er'uoY");
//transform an argument string into all lowercase letters 
echo strtolower("SOON, tHiS WILL Look NoRmAL.");
//string repeated the argument number of times 
echo str_repeat("\nThere's no place like home.\n", 3); 

$text_one = "I really enjoyed the book. I thought the characters were really interesting.";
echo substr_count($text_one, "really");

// *Number Functions: -->

function calculateDistance($num_1, $num_2){
  return abs($num_1 - $num_2);
}
echo calculateDistance(-1, 4);

function calculateTip_two($total)
{
  return round($total * 1.18);
}
echo calculateTip_two(187);

echo rand(1, 52); //random number

function convertToShout($str){
  return strtoupper($str) . "!";
}
echo convertToShout("i just don't know");

function tipGenerously($cost){
  return ceil($cost * 1.2);
}
echo tipGenerously(66.18);

function calculateCircleArea($diameter)
{
  return pi() * ($diameter/2)**2;
}
echo calculateCircleArea(29);

// !Anonymous functions: -->

$list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];

function map($array, $func){
  $newList = [];
  foreach($array as $key => $item){
    $newList[$key] = $func($item);
  }

  return $newList;
}

$list_two = map($list, function($num){
  return $num ** 2;
});
echo $list_two;

// !Arrow Functions: --> 
// one line function

$list_three = map($list, fn($num) => $num * 2);
echo $list_three;

$c = 5;
$sum = fn($a, $b) => $a + $b + $c;
echo $sum(2, 5);




# Classes and Objects ==>

class Beverage {
  public $color, $opacity, $temperature;
  function getInfo(){
    return "This beverage is " . $this->temperature . " and " . $this->color . ".";
}}

$tea = new Beverage();
$tea->temperature = 'hot';
echo $tea->temperature;

// !Methods: -->
$soda = new Beverage();
$soda->color = "black";
$soda->temperature = "cold";
echo $soda->getInfo();

// !Constructor Method: -->
class Pet {
  public $name;
  private $healthScore = 0; // !Visibility - Private Members
  protected $opacity; // !Visibility - Protected Members
  function __construct($name) {
    $this->name = $name;
  }
} 
$dog = new Pet("Lassie");
echo $dog->name;

// !Inheritance: -->
class Milk extends Beverage {
  function __construct() {
    $this->temperature = "cold";
  }
  function getInfo() {
  return parent::getInfo() . " I like my milk this way.";
  }
}

$milk = new Milk();
echo $milk->getInfo();

// !Getters and Setters: -->
class Drink {
  private $color;
  function setColor($color) {
    $this->color = strtolower($color);
  }
  function getColor() {
    return $this->color;
  }
}

$coffee = new Drink();

// !Static Members: -->
class AdamsUtils {
  public static $the_answer = 42;
  public static function addTowel($string) {
    return $string . " and a towel.";
  }
}

$items = "I brought apples";
echo AdamsUtils::$the_answer;
echo "\n";
echo AdamsUtils::addTowel($items);